/*
[SECTION] Arrays and Indices

-Arrays are used to store multiple related values in a single variable
-They are declared using square brackets ([]), also known as array literals
*/

let grades = [98.5, 94.3, 89.2, 90.1]

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu', 'Neo']

let tasks = [
	"shower",
	"eat breakfast",
	"go to work",
	"go home",
	"go to sleep"
]

/*
[SECTION] Reading from Arrays
-Accessing array elements is one of the more common tasks that we perform
-This can be done through the use of array indices
-Each element in an array is associated with its own index/number
-In JavaScript, the first element inside of an array has the index number of zero/0
-Succeeding elements have their index number increase by 1 (so the second element would have an index of 1, the third would be 2, the fourth would be 3, etc)
-The syntax to access an array element is:

arrayName[index]
*/

// console.log(grades[0])
// console.log(computerBrands[3])
// console.log(tasks[4])

//Reassigning array element values
// console.log('Array before reassignment:')
// console.log(tasks)
// tasks[4] = "play games"
// console.log('Array after reassignment:')
// console.log(tasks)

/*
Getting the length of an array

-Arrays have a .length property similar to strings to get the number of elements inside of an array
*/

// let lastIndex = computerBrands.length - 1

// console.log(computerBrands[lastIndex])

/*
[SECTION] Array Methods

Mutator methods:

-Mutator methods are functions that "mutate" or change an array after they are used
-These methods/functions manipulate the original array by performing various tasks
*/

/*
.push()

-Adds an element or multiple elements to the end of an array AND returns the array's new length
*/

// grades.push(85.5, 87.7)

// console.log(grades)

/*
.pop()

-Removes the last element of an array AND returns the removed element
*/

// let removedGrade = grades.pop()

// console.log(grades)

// grades.push(removedGrade)

// console.log(grades)

/*
.unshift()
-Adds one or more elements to the beginning of an array
*/

// grades.unshift(77.7, 88.8)

// console.log(grades)

/*
.shift()
-Removes an element at the beginning of an array
*/

// grades.shift()

// console.log(grades)

/*
.splice()

-Can add, remove, or add AND remove simultaneously

Syntax: .splice(starting index number, number of elements to remove, elements to add *optional)
*/

// console.log("Before splice:")
// console.log(tasks)
// tasks.splice(3, 0, "eat lunch", "eat dessert")
// console.log("Add with splice:")
// console.log(tasks)
// tasks.splice(2, 1)
// console.log("Remove with splice:")
// console.log(tasks)
// tasks.splice(3, 1, "code")
// console.log("Remove AND add with splice:")
// console.log(tasks)

/*
.reverse()
-Reverses the order of array elements
*/

// tasks.reverse()
// console.log(tasks)

/*
.sort()
-Rearranges the array elements in alphanumeric order
*/

// tasks.sort()
// console.log(tasks)

/*
Non-Mutator Methods
-Non-Mutator methods are functions that do not modify or change the original array
*/

/*
.indexOf()
-Returns the number of the FIRST matching element found in an array
-If not match was found, returns -1
*/

// console.log(computerBrands.indexOf("Neo"))
// console.log(computerBrands.indexOf("Gigabyte"))

/*
.lastIndexOf()
-Returns the number of the LAST matching element found in an array
*/

// console.log(computerBrands.lastIndexOf("Neo"))

// if(computerBrands.indexOf("Gigabyte") === -1){
// 	console.log("Walang Gigabyte")
// }

/*
.slice()
-Copies a portion of an array to a new array

Syntax: .slice(starting index, ending index *not included/optional)

If no second argument is passed, continues until the end of the array

If a negative number is passed as the first argument, index count begins from the end of the array
*/

// let numbersA = [1, 2, 3, 4, 5]

// let numbersB = numbersA.slice(-3, 3)

// console.log(numbersA)
// console.log(numbersB)

/*
.toString()
-Returns the array elements as a string separated by commas
*/

// console.log(computerBrands.toString())
// console.log(typeof computerBrands.toString())

/*
.join()
-Returns the array elements as a string separated by commas. A string passed as an argument becomes a separator for each array element.
*/

// console.log(computerBrands.join(" "))
// console.log(typeof computerBrands.join())

let numbersA = [1, 2, 3, 4, 5]
let numbersB = [6, 7, 8, 9, 10]
let numbersC = [11, 12, 13, 14, 15]

/*
.concat()
-Return a new array populated with the combined results of the two (or more) given arrays
*/

let numbersD = numbersA.concat(numbersB, numbersC)
// let numbersD = numbersA.concat(numbersB.concat(numbersC))

// console.log(numbersA)
// console.log(numbersB)
// console.log(numbersC)
// console.log(numbersD)

/*

	Create an array of students with the following value:
	let students = ["John", "Jane", "Joe"];
	Find the following information using array methods: mutators and accessors: 

*/

let students = ["John", "Jane", "Joe"];

// 	1.) Total size of an array.
// console.log("The total size of the array is " +students.length);

// 2.) Adding a new element at the beginning of an array. (Add Jack)
// students.unshift("Jack");
// console.log("Jack is added in the beginning of the array:");
// console.log(students);

// 3.) Adding a new element at the end of an array. (Add Jill);
// students.push("Jill");
// console.log("Jill is added in the end of the array:");
// console.log(students);

// 4.) Removing an existing element at the beginning of an array.
// students.shift();
// console.log("Jack was removed in the beginning of the array:");
// console.log(students);

// 5.) Removing an existing element at the end of an array.
// students.pop();
// console.log("Jill was removed in the end of the array:");
// console.log(students);

// 6.) Find the index number of the last element in an array.
// console.log("The last index of the array is: " +(students.length-1));

// 7.) Print each element of the array in the browser console.
					//3
					// 3 < 3 - false
	// for(i = 0; i < students.length; i++){
	// 	console.log(students[i]);
	// }

	// Array Method (iteration)

	// students.forEach(function(student){
	// 	console.log(student);
	// })

// Iteration Methods
// - Iteration methods are loops designed to perform repetitive tasks on array.
// - Useful for manipulating array data resulting in complex task.

// forEach()
// similar to a for loop that iterates on each array element.
/*
	
	Syntax:
		arrayName.forEach(function(indivElement){
			//statement/code block
		})

*/

// Using forEach with conditional statements
// let filteredTasks = [];

// tasks.forEach(function(task){
// 	// "task" parameter represents each element of the "tasks" array.
// 	// console.log(task);

// 	// if the elements length is greater than 10 characters it will be stored inside the filteredTasks variable.
// 	if(task.length > 10){
// 		// add element to the filteredTasks Array
// 		filteredTasks.push(task);
// 	}
// })

// console.log("Results of filteredTasks:");
// console.log(filteredTasks);

// map()
// Iterates on each element AND returns new array with different values depending on the result of the function's operation.
// Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation.
/*
	
	let/const resultArray = arrayName.map(function(indivElement){
		//statement/code block
		return statement
	})

*/

	/*
			forEach() vs map()

			- "forEach()" is used to run a function without changing the array and returning any value. 
			- "map()" is used to transform each element of an array and store it into a new array.

	*/

let numbers = [1, 2, 3, 4, 5];

// let numberMap = numbers.map(function(number){
// 	return number * number;
// });

// console.log(numbers);
// console.log("Result of map method:");
// console.log(numberMap);

// let taskMap = tasks.map(function(task){
// 	if(task.length > 10){
// 		return task;
// 	}
// })

// console.log(taskMap);

// every()
// checks if all elements in an array meet the given condition.
// Return a "true" value if all elements meet the condition
// let allValid = numbers.every(function(number){
// 	return (number < 3);
// })

// console.log("Result of every method:");
// console.log(allValid);

// some()
// check if at least one element in the array meets the given condition.
// Return a "true" value if one element meets the condition.
// let someValid = numbers.some(function(number){
// 	return (number < 3);
// })

// console.log("Result of some method:")
// console.log(someValid);

// Combining the returned result from every/some method may be used in other statements to perform consecutive results.
// if(someValid){
// 	console.log("Some numbers in the array are greater than 3");
// }

// filter()
// - Returns new array that contains elements which meets the given condition.
// - Return an empty array if no elements were found.
/*

	let/const resultArray = arrayName.filter(function(indivElement){
		return expression/condition (element that meets the condition)
	})

*/

// Filtering using forEach()
// let filteredNumbers = [];

// numbers.forEach(function(number){
// 	if(number < 3){
// 		filteredNumbers.push(number);
// 	}
// })

// console.log("Result of filterNumbers using forEach:");
// console.log(filteredNumbers);

// let filterValid = numbers.filter(function(number){
// 	return (number < 3);
// });

// console.log("Result of filter method:");
// console.log(filterValid);

/*

	let filteredTasks = [];

	tasks.forEach(function(task){

		if(task.length > 10){
			filteredTasks.push(task);
		}
	})

	console.log(filteredTasks);

	Refactor the code above using filter() method.
	once done, send a screenshot of your working code in the batch hangouts.

*/

// let taskFilter = tasks.filter(function(task){
// 	return (task.length > 10);
// })

// console.log(taskFilter);

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

// includes()
// determines whether an array includes a certain value among array elements.
// Return Boolean & it is case sensitive

// console.log(products);
// console.log(products.includes('Mouse'));

// includes using string
// check if the character or series of characters exist in the string
// let word = "Hello"
// console.log(word.toLowerCase().includes('hel'));

// Methods can be "chained"
// - it can be used one after another.
// inside method before outside method

// if we have mouse inside the product array
let filteredProducts = products.filter(function(product){
	// product paramater is string
	// return (product.toLowerCase() == 'mouse');
	/*
		using includes, it checks if an array element contains letter 'a'
		Mouse = false
		Keyboard = true
		Laptop = true
		Monitor = false

	*/
	// console.log(product.toLowerCase().includes('a'));
	return product.toLowerCase().includes('a');
})

console.log(filteredProducts);

// reduce()
// - Evaluates elements from left to right and returns/reduces the array into a single value.
/*
	Syntax:
		let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression/operation
		})

		- accumulator paramater stores the result for every iteration of the loop.
		- currentValue current/next element in the array that is evaluated in each iteration of the loop.

*/

let iteration = 0;

// let reducedArray = numbers.reduce(function(acc, cur){
// 	// used to track the current iteration count
// 	console.warn('current iteration: '+ iteration++);
// 	console.log('accumulator: ' +acc);
// 	console.log('currentValue: ' +cur);

// 	return acc * cur;
// })
// 										// 1+2+3+4+5
// console.log("Result of reduce method: " +reducedArray);

// reducing string array

let list = ["Hello", "Again", "My","World"];

let reduceJoin = list.reduce(function(x, y){
	console.warn('current iteration: '+ iteration++);
	console.log('accumulator: ' +x);
	console.log('currentValue: ' +y);
	return x + " " + y;
})
console.log("Result of reduce method in string:" +reduceJoin);

let students = [];  

function addStudent(name) {

  students.push(name); 
  console.log(name + "was been added to the student's list");
}

addStudent('John');
addStudent('Jane');
addStudent('Joe'); 
console.log(students); 

function countStudents() {
  console.log('This Class has a total of '+ students.length +' enrolled students');
} 
countStudents(); 


function printStudents() {
   students = students.sort();
   students.forEach(function(student) {
   	 console.log(student); 
   })
}

printStudents(); 


function findStudent(keyword) {
  let matches = students.filter(function(student) { 
    return student.toLowerCase().includes(keyword.toLowerCase());   
  }); 

  if (matches.length === 1) {
  	console.log(matches[0] + ' is an enrollee');
  } else if (matches.length > 1 ){
  	console.log(students + '  are enrollees');
  } else {
  	console.log("Student name is not an enrollee.");
  }
}
